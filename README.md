# Sneaky Screens

[![GitHub release](https://img.shields.io/github/release/haykam821/Sneaky-Screens.svg?style=popout&label=github)](https://github.com/haykam821/Sneaky-Screens/releases/latest)
[![CurseForge](https://img.shields.io/static/v1?style=popout&label=curseforge&message=project&color=6441A4)](https://www.curseforge.com/minecraft/mc-mods/sneaky-screens)
[![Discord](https://img.shields.io/static/v1?style=popout&label=chat&message=discord&color=7289DA)](https://discord.gg/jDXyC4Y)

Persists the sneak state in screens.
